#ifndef TRANS_VIEW_H
#define TRANS_VIEW_H
#include <iostream>
#include <vector>
#include "TimeSlot.h"

#define COMMAND_KW "request_transportation"
//command_format:
    // request_transportation volume week_id


class TransportationController;

class TransportationView{
private:
    TransportationController* mTransportationController;
    std::vector<std::string> parse_command(std::string command);
public:
    void setTransportationController(TransportationController* tc){
        mTransportationController = tc;
    }
    void run();
    void showTimeSlots(const std::vector<TimeSlot*> TimeSlots, const int estimatedPrice);
};

#endif