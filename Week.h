#ifndef WEEK_H
#define WEEK_H
#include "TimeSlot.h"
#include <vector>

class Week{
private:
    int weekId;
    std::vector<TimeSlot*> TimeSlots;
public:
    Week(const std::vector<TimeSlot*> initialTimeSlots, const int _weekId) { this->TimeSlots = initialTimeSlots; this->weekId = _weekId;}
    std::vector<TimeSlot*> getFreeTimeSlot(){
        std::vector<TimeSlot*> FreeTimeSlots;
        for(auto TimeSlot = this->TimeSlots.begin(); TimeSlot != this->TimeSlots.end(); ++TimeSlot){
            if(!(*TimeSlot)->isOccupied())
                FreeTimeSlots.push_back((*TimeSlot));
        }
        return FreeTimeSlots;
    }
    std::vector<TimeSlot*> getAllTimeSlots() const {return this->TimeSlots;}
    int getWeekId() const { return this->weekId; }
};

#endif