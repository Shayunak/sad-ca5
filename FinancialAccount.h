#ifndef FIN_ACC_H
#define FIN_ACC_H

#include <vector>
#include "Receipt.h"

class FinancialAccount{
    public:

    FinancialAccount(){}
    void addReceipt(Receipt receipt){
        ledger.push_back(receipt);
    }

    private:

    std::vector<Receipt> ledger;

};

#endif
