#ifndef TRANS_CONTROL_H
#define TRANS_CONTROL_H
#include "TransportationView.h"
#include "TimeSlot.h"
#include "TimeTable.h"
#include "PriceEstimator.h"
#include "PricePolicy.h"
#include "FinancialAccount.h"
#include "PaymentGate.h"
#include <exception>

class NoFreeTimeSlotsException : public std::exception{
    virtual const char* what() const throw(){
        return "There is no free time slot left!";
    }
};


class TransportationController{
private:
    TimeTable* TimeTableAccess;
    PriceEstimator* PriceEstimatorAccess;
    PricePolicy* PricePolicyAccess;
    TransportationView* mTransportationView;
    FinancialAccount* FinancialAccountAccess;
    PaymentGate* PaymentGateAccess;
    std::vector<TimeSlot*> checkTimeSlot(int weekId);
public:
    TransportationController(TimeTable* _TimeTableAccess, PriceEstimator* _PriceEstimatorAccess, PricePolicy* _PricePolicyAccess,
        TransportationView* _TransportationView, FinancialAccount* _FinancialAccountAccess, PaymentGate* _PaymentGateAccess);
    void requestTransportation(int volume, int transportationWeek);
    void selectTimeSlot(TimeSlot* selectedTimeSlot, const int deposit);
};

#endif