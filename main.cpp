#include <iostream>
#include "TransportationView.h"
#include "TransportationController.h"
#include "FinancialAccount.h"
#include "PaymentGate.h"
#include "PriceEstimator.h"
#include "PricePolicy.h"
#include "TimeTable.h"
#include "TimeTableDAO.h"

using namespace std;

int main(){
    TimeTable* timeTableDAO = new TimeTable();
    timeTableDAO->loadTimeTable("time_table_file.txt");
    PriceEstimator* priceEstimator = new PriceEstimator();
    PricePolicy* pricePolicy = new PricePolicy();
    FinancialAccount* financialAccount = new FinancialAccount();
    PaymentGate* paymentGate = new PaymentGate("pay.com", "127.195.1.1");
    TransportationView* transportationView = new TransportationView();

    TransportationController* transportationController = new TransportationController(timeTableDAO, priceEstimator, pricePolicy,
        transportationView, financialAccount, paymentGate);

    transportationView->setTransportationController(transportationController);
    
    transportationView->run();

    timeTableDAO->saveTimeTable("time_table_file.txt");
}