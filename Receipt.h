#ifndef __RECE_H
#define __RECE_H

#include <vector>
#include <string>
#include <ctime>

class Receipt{
    public:

    Receipt(int _id, std::string _bankTransactionCode, double _fee){
        fee = _fee;
        timeVerified = std::time(NULL); // some function lateron to get sytem time
        id = _id;
        bankTransactionCode = _bankTransactionCode;
    }

    private:
    
    int id;
    std::string bankTransactionCode;
    long int timeVerified;
    double fee;
};

#endif
