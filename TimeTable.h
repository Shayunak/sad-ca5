#ifndef TIME_TABLE_H
#define TIME_TABLE_H

#include "Week.h"
#include "TimeSlot.h"
#include "TimeTableDAO.h"
#include <vector>
#include <string>

class TimeTable : public TimeTableDAO
{
private:
    std::vector<Week*> wk;
    std::vector<bool> split_day(std::string day);
public:
    std::vector<TimeSlot*> getFreeTimeSlot(int transportationWeek);
    Week* getWeek(int transportationWeek);
    virtual void loadTimeTable(std::string fileName);
    virtual void saveTimeTable(std::string fileName);
};

/*File Format:
1
0 1 1 : Saturday
0 1 0 : Sunday
...
1 0 0 : Friday
2
...
*/
#endif