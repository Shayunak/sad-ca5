#include "TimeTable.h"
#include <fstream>
#include <sstream>

using namespace std;

vector<TimeSlot*> TimeTable::getFreeTimeSlot(int transportationWeek){
    Week* week = this->getWeek(transportationWeek);
    return week->getFreeTimeSlot();
}

Week* TimeTable::getWeek(int transportationWeek){
    for (unsigned int i = 0; i < wk.size(); i++)
        if (wk[i]->getWeekId() == transportationWeek)
            return wk[i];

    return nullptr;
}

vector<bool> TimeTable::split_day(string day){
    vector<bool> timeOfDays;
    istringstream ss(day);
    string timeOfDay;
    while (ss >> timeOfDay)
        timeOfDays.push_back((bool) stoi(timeOfDay));
    
    return timeOfDays;
}

void TimeTable::loadTimeTable(string fileName){
    string NameOfDays[7] = {"Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"};
    string NameOfTimeOfDay[3] = {"Morning", "Afternoon", "Evening"};
    fstream DataFile;
    DataFile.open(fileName, ios::in);
    string week;
    while(getline(DataFile, week)){
        string day;
        vector<TimeSlot*> timeSlots;
        for(unsigned int i = 0; i < 7; ++i){
            getline(DataFile, day);
            vector<bool> timeOfDays = split_day(day);
            for(unsigned int j = 0; j < timeOfDays.size(); ++j)
                timeSlots.push_back(new TimeSlot(NameOfDays[i], NameOfTimeOfDay[j], timeOfDays[j]));
        }
        this->wk.push_back(new Week(timeSlots, stoi(week)) );
    }
    DataFile.close();
}

void TimeTable::saveTimeTable(string fileName){
    fstream DataFile;
    DataFile.open(fileName, ios::out);
    for(unsigned int i = 0; i < this->wk.size(); ++i){
        DataFile << wk[i]->getWeekId() << endl;
        vector<TimeSlot*> timeSlots = wk[i]->getAllTimeSlots();
        for(unsigned int j = 0; j < 7; ++j)
            DataFile << to_string((int) timeSlots[3*j]->isOccupied()) << " " << to_string((int) timeSlots[3*j+1]->isOccupied()) << 
                " " << to_string((int) timeSlots[3*j+2]->isOccupied()) << endl; 
    }
    DataFile.close();
}