#ifndef PAY_GATE_H
#define PAY_GATE_H

#include "Receipt.h"
#include <vector>
#include <string>

class PaymentGate{
    public:

    PaymentGate(std::string _url, std::string _connection){
        url = _url;
        connection = _connection;
    }

    Receipt Pay(double amount){
        url = makePaymentGate(amount);
        std::string bankTransactionCode = forwardToPaymentGate();
        return makeReciept(bankTransactionCode, amount, max_id++);
    }
    
    private:
    
    Receipt makeReciept(std::string bankTransactionCode, double fee, int id){
        return Receipt(id, bankTransactionCode, fee);
    }
    std::string forwardToPaymentGate(){ return "someTXcode"; }
    std::string makePaymentGate(double amount){ return "some url"; }

    int max_id = 0;
    std::string url;
    std::string connection = "some connection";
};

#endif