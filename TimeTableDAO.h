#ifndef DAO_H
#define DAO_H

#include <string>
#include "TimeTable.h"

class TimeTableDAO{
public:
    virtual void loadTimeTable(std::string fileName) = 0;
    virtual void saveTimeTable(std::string fileName) = 0;
};

#endif