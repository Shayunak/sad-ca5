#include "TransportationController.h"
#include "TimeSlot.h"
#include "Receipt.h"

using namespace std;

TransportationController::TransportationController(TimeTable* _TimeTableAccess, PriceEstimator* _PriceEstimatorAccess, PricePolicy* _PricePolicyAccess,
        TransportationView* _TransportationView, FinancialAccount* _FinancialAccountAccess, PaymentGate* _PaymentGateAccess){
    
    this->TimeTableAccess = _TimeTableAccess;
    this->PriceEstimatorAccess = _PriceEstimatorAccess;
    this->PricePolicyAccess = _PricePolicyAccess;
    this->mTransportationView = _TransportationView;
    this->FinancialAccountAccess = _FinancialAccountAccess;
    this->PaymentGateAccess = _PaymentGateAccess;
}

vector<TimeSlot*> TransportationController::checkTimeSlot(int weekId){
    vector<TimeSlot*> FreeTimeSlots = TimeTableAccess->getFreeTimeSlot(weekId);
    if(FreeTimeSlots.size() == 0)
        throw new NoFreeTimeSlotsException();
    return FreeTimeSlots;
}

void TransportationController::requestTransportation(int volume, int transportationWeek){
    vector<TimeSlot*> FreeTimeSlots = checkTimeSlot(transportationWeek);
    int estimatedPrice = PriceEstimatorAccess->estimatePrice(volume, PricePolicyAccess);
    mTransportationView->showTimeSlots(FreeTimeSlots, estimatedPrice);
}

void TransportationController::selectTimeSlot(TimeSlot* selectedTimeSlot, const int deposit){
    selectedTimeSlot->reserveSlot();
    Receipt NewReceipt = PaymentGateAccess->Pay(deposit);
    FinancialAccountAccess->addReceipt(NewReceipt);
    cout << "your slot successfully reserved!\n";
}