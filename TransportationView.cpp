#include "TransportationView.h"
#include "TransportationController.h"
#include <string>
#include <sstream>

using namespace std;


vector<string> TransportationView::parse_command(string command){
    vector<string> command_parts;
    istringstream ss(command);
    string part;
    while (ss >> part)
        command_parts.push_back(part);
    
    return command_parts;
}

void TransportationView::run(){
    string line;
    vector<string> command_parts;
    cout << "please enter your transportation request with estimated volume and week in year:) \n";
    while(getline(cin, line)){
        command_parts = parse_command(line);
        if(command_parts.size() != 3 || command_parts[0] != COMMAND_KW)
            cout << "The command is not in a proper format!" << endl;
        else{
           try{
               mTransportationController->requestTransportation(stoi(command_parts[1]), stoi(command_parts[2]));
           }catch(const exception& exp){
               cout << exp.what() << endl;
           }
        }
        cout << "please enter your transportation request with estimated volume and week in year:) \n";
    }
}

void TransportationView::showTimeSlots(const vector<TimeSlot*> TimeSlots, const int estimatedPrice){
    int selectedIndex;
    string line;
    for(int i = 0; i < TimeSlots.size(); i++)
        cout << i+1 << ". On " << TimeSlots[i]->getDay() << " : in the " << TimeSlots[i]->getTimeOfDay() << endl;
    cout << "EstimatedPrice: " << estimatedPrice << endl;
    cout << "Select A Time Slot: ";
    getline(cin, line);
    // cin >> selectedIndex;
    selectedIndex = stoi(line);
    mTransportationController->selectTimeSlot(TimeSlots[selectedIndex-1], estimatedPrice);
}