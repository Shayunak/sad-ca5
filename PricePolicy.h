#ifndef PRICE_POL_H
#define PRICE_POL_H


// 1 is 1000 tomans
#include <map>
#include <vector>

class PricePolicy
{
private:
    std::map<int, double> depositPolicy;
    // std::map<std::pair<int, int>, int> pricePolicy;
    int price_man_per_slot;
    int price_truck_per_slot;
    std::vector<int> hotPricePoints = {100, 500, 1000};
public:
    PricePolicy(){
        depositPolicy[hotPricePoints[0]] = .2;
        depositPolicy[hotPricePoints[1]] = .25;
        depositPolicy[hotPricePoints[2]] = .3;

        price_man_per_slot = 100;
        price_truck_per_slot = 500;
    }
    int getPrice(int manPower, int truckNum){ return manPower * price_man_per_slot + truckNum * price_truck_per_slot; }
    void updatePolicyMan(int newval){ price_man_per_slot = newval; }
    void updatePolicyTruck(int newval){ price_truck_per_slot = newval; }
    int getPriceDeposit(int price){
        int i = 0;
        while(hotPricePoints[i] < price)
            i++;
        i--;
        return price * depositPolicy[hotPricePoints[i]];
    }
};

#endif