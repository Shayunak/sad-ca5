CC := g++ --std=c++11

all: TransportationRequest.out clean 

TransportationRequest.out: main.o TimeTable.o TransportationController.o TransportationView.o
	$(CC) main.o TimeTable.o TransportationController.o TransportationView.o -o TransportationRequest.out

main.o: main.cpp FinancialAccount.h PaymentGate.h PriceEstimator.h PricePolicy.h Receipt.h TimeSlot.h TimeTable.h TimeTableDAO.h TransportationController.h TransportationView.h Week.h
	$(CC) -c main.cpp -o main.o

TimeTable.o: TimeTable.cpp TimeTable.h
	$(CC) -c TimeTable.cpp -o TimeTable.o

TransportationView.o: TransportationView.cpp TransportationView.h TransportationController.h
	$(CC) -c TransportationView.cpp -o TransportationView.o

TransportationController.o: TransportationController.cpp TransportationController.h TimeSlot.h Receipt.h
	$(CC) -c TransportationController.cpp -o TransportationController.o

.PHONY: clean
clean:
	rm -r *.o