#ifndef TIME_SLOT_H
#define TIME_SLOT_H

#include <string>

class TimeSlot
{
private:
    std::string day;
    std::string timeOfDay;
    bool occupied;
public:
    TimeSlot(std::string _day, std::string _timeOfDay, bool _occupied) : day(_day),  timeOfDay(_timeOfDay), occupied(_occupied){}
    bool isOccupied(){return occupied;}
    std::string getDay(){return day;}
    std::string getTimeOfDay(){return timeOfDay;}
    void reserveSlot(){occupied = true;}
};

#endif