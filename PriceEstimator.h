#ifndef PRICE_EST_H
#define PRICE_EST_H

#include "PricePolicy.h"

// each volume is almost a truck load

class PriceEstimator
{
private:
    int manPower_per_volume = 4;
    int truck_per_volume = 1;
    int estimateManPower(int volume){ return volume * manPower_per_volume; }
    int estimateTruckNum(int volume){ return volume * truck_per_volume; }
public:
    PriceEstimator(){};
    int estimatePrice(int volume, PricePolicy* ppl){ 
        return ppl->getPrice(estimateManPower(volume), estimateTruckNum(volume));
    }
};


#endif